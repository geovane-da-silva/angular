import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';


@Component({
  selector: 'app-list-destino',
  templateUrl: './list-destino.component.html',
  styleUrls: ['./list-destino.component.css']
})
export class ListDestinoComponent implements OnInit {
destino: DestinoViaje[];
  constructor() {
  this.destino = [];

 


   }

  ngOnInit() {
  }
  guardar(nombre:string, url:string) :boolean {
  this.destino.push(new DestinoViaje(nombre, url));
  return false;
  }

}
