import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListDestinoComponent } from './list-destino/list-destino.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListDestinoComponent,
    DestinoDetalleComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreDevtoolsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
